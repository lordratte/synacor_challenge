⎕IO ← 0

filename ← 'challenge.bin'

⍝ Enable boxing on terminal
(⎕NS⍬).(_←enableSALT⊣⎕CY'salt')
]box on -s=max


memory ← 32768⍴0
registers ← 8⍴0
stack ← ⍬

CR ← ⎕UCS 10


initial_pointer ← 0
first_reg_index ← 32768


input_buffer ← ⍬
commands ← 'take tablet' 'doorway' 'north' 'north' 'bridge' 'continue' 'down' 'east' 'take empty lantern' 'west' 'west' 'passage'

commands ,← 'ladder' 'west' 'south' 'north' 'take can' 'use can' 'use lantern' 'west'

commands ,← 'ladder' 'darkness' 'continue' 'west' 'west' 'west' 'west' 'north' 'take red coin' 'north' 

commands ,← 'west' 'take blue coin' 'up' 'take shiny coin' 'down' 'east'  'east' 'take concave coin' 'down' 'take corroded coin' 'up' 'west'


⍝ This section was used to brute force the correct sequence of coins
⍝ ===================================
⍝reset ←  'take red coin' 'take blue coin' 'take shiny coin' 'take concave coin' 'take corroded coin' 'north'
⍝
⍝uses ←  'use red coin' 'use blue coin' 'use shiny coin' 'use concave coin' 'use corroded coin'
⍝
⍝⎕IO←1
⍝perms ← {uses[⍵]}¨⍉↓  {0=⍵:1 0⍴0 ⋄ ,[⍳2](⍒⍤1∘.=⍨⍳⍵)[;1,1+∇ ⍵-1]}5
⍝⎕IO←0
⍝
⍝commands ,←⊃{⍺,reset,⍵}/perms
⍝
⍝commands ,← 'look' 'north'

⍝ ===================================


commands ,← 'use blue coin' 'use red coin' 'use shiny coin' 'use concave coin' 'use corroded coin' 'north' 'take teleporter' 'use teleporter'

⍝commands ,← 'take business card' 'take strange book' 'look business card' 'look strange book' 'use teleporter'

input_buffer ,← ⊃{⍺,CR,⍵}/commands
input_buffer ,← CR


⍝ Load binary into memory
raw_input ← ((⎕NUNTIE⊢⎕NREAD⍤,∘163 ¯1)⎕NTIE∘0)filename

memory[⍳⍴raw_input] ← raw_input

memory ← {⍵<0:⍵+2*16 ⋄ ⍵}¨memory


⍝== utils ==

deref ← {⍵<first_reg_index:⍵ ⋄ registers[⍵-first_reg_index]}

bin_store ← {
    vals ← deref¨memory[⍵+2+⍳2]

    reg_set ← memory[⍵+1]-first_reg_index
    registers[reg_set] ← ⍺⍺/vals
    ⍵+4
}

panic ← {
    ⎕←⍵
    ⎕OFF
}

⍝== operations ==

⍝ 0: halt: stop execution and terminate the program
halt ← {⎕OFF}

⍝ 1: set: set register <a> to the value of <b>
set ← {
    registers[memory[⍵+1]-first_reg_index] ← deref memory[⍵+2]
    ⍵+3
}

⍝ 2: push: push <a> onto the stack
push ← {
    stack,←deref memory[⍵+1]
    ⍵+2
}

⍝ 3: pop: remove the top element from the stack and write it into <a>; empty stack = error
pop ← {
    ⍬≡stack: panic 'Error, empty stack'
    registers[memory[⍵+1]-first_reg_index] ← ⊃⌽stack
    stack {¯1∘↓⍺}← 0 
    ⍵+2
}

⍝ 4: eq: set <a> to 1 if <b> is equal to <c>; set it to 0 otherwise
eq ← ≡bin_store 

⍝ 5: gt: set <a> to 1 if <b> is greater than <c>; set it to 0 otherwise
gt ← >bin_store 

⍝ 6: jmp: jump to <a>
jmp ← {
    deref memory[⍵+1]
}

⍝ 7: jt: if <a> is nonzero, jump to <b>
jt ← {
    vals ← deref¨memory[⍵+1+⍳2]
    0≢⍨⊃vals:⊃⌽vals
    ⍵+3
}

⍝ 8: jf: if <a> is zero, jump to <b>
jf ← {
    vals ← deref¨memory[⍵+1+⍳2]
    0≡⍨⊃vals:⊃⌽vals
    ⍵+3
}

⍝ 9: add: assign into <a> the sum of <b> and <c> (modulo 32768)
add ← {first_reg_index | ⍺ + ⍵}bin_store

⍝ 10: mult: store into <a> the product of <b> and <c> (modulo 32768)
mult ← {first_reg_index | ⍺ × ⍵}bin_store

⍝ 11: mod: store into <a> the remainder of <b> divided by <c>
mod ← {⍵|⍺}bin_store

⍝ 12: and: stores into <a> the bitwise and of <b> and <c>
and ← {⍉2⊥∧/2⊥⍣¯1⍉⍺,⍵}bin_store


⍝ 13: or: stores into <a> the bitwise or of <b> and <c>
or ← {⍉2⊥∨/2⊥⍣¯1⍉⍺,⍵}bin_store

⍝ 14: not: stores 15-bit bitwise inverse of <b> in <a>
not ← {
    registers[memory[⍵+1]-first_reg_index] ← {2⊥~{(0⍴⍨15-⍴⍵),⍵}2⊥⍣¯1⍉⍵}deref memory[⍵+2]
    ⍵+3
}

⍝ 15: rmem: read memory at address <b> and write it to <a>
rmem ← {
    registers[memory[⍵+1]-first_reg_index] ← memory[deref memory[⍵+2]]
    ⍵+3
}

⍝ 16: wmem: write the value from <b> into memory at address <a>
wmem ← {
    memory[deref memory[⍵+1]] ← deref memory[⍵+2]
    ⍵+3
}

⍝ 17: call: write the address of the next instruction to the stack and jump to <a>
call ← {
    stack,←⍵+2
    deref memory[⍵+1]
}

⍝ 18: ret: remove the top element from the stack and jump to it; empty stack = halt
ret ← {
    ⍬≡stack: halt ⍵
    top←⊃⌽stack
    stack {¯1∘↓⍺}← 0
    top
}

⍝ 19: out:  write the character represented by ascii code <a> to the terminal
out ← {
    ⍞←⎕UCS deref memory[⍵+1]
    ⍵+2
}

⍝ 20: in: read a character from the terminal and write its ascii code to <a>; it can be assumed that once input starts, it will continue until a newline is encountered; this means that you can safely read whole lines from the keyboard and trust that they will be fully read
in ← {
    registers[memory[⍵+1]-first_reg_index] ← {
        0≡≢input_buffer:{
            input_buffer ,←⍞
            input_buffer ,← ⎕UCS 10
            ⎕UCS ⊃input_buffer
        } ⍬
        ⎕UCS ⊃input_buffer
    } ⍬
    input_buffer {1∘↓⍺}← ⍬
    ⍵+2
}

⍝ 21: noop: do nothing
noop ← {⍵+1}

ops ← {
    ⍵ = 0: halt ⍺ 
    ⍵ = 1: set ⍺
    ⍵ = 2: push ⍺
    ⍵ = 3: pop ⍺
    ⍵ = 4: eq ⍺
    ⍵ = 5: gt ⍺
    ⍵ = 6: jmp ⍺
    ⍵ = 7: jt ⍺
    ⍵ = 8: jf ⍺
    ⍵ = 9: add ⍺
    ⍵ = 10: mult ⍺
    ⍵ = 11: mod ⍺
    ⍵ = 12: and ⍺
    ⍵ = 13: or ⍺
    ⍵ = 14: not ⍺
    ⍵ = 15: rmem ⍺
    ⍵ = 16: wmem ⍺
    ⍵ = 17: call ⍺
    ⍵ = 18: ret ⍺
    ⍵ = 19: out ⍺
    ⍵ = 20: in ⍺
    ⍵ = 21: noop ⍺
    panic 'Error, unimplimented operation: '⍵ 
}

{
    ⍝ Fetch the next operation code
    op ← memory[⍵]

    ⍝ Execute the operation
    new_pointer ← ⍵ ops op

    ⍝ Reiterate with the new pointer until the halt operation is reached
    op ≠ 0:∇ new_pointer
} initial_pointer
