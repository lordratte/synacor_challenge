### Warning: Code contains spoilers

# APL Soultion to the Synacor Challenge

The original repo that defined the challenge can be found [here](https://github.com/Aneurysm9/vm_challenge).

The local copy of the README is [here](./upstream-README.md) and the local copy of the challenge specification is [here](./arch-spec).

## Usage

Provided you have a working Dyalog APL interpreter, you can run the script with the following command:

~$ `dyalogscript.bash challenge.bin`

The code adds hardcoded data to the standard input. Set the variable `commands` to an empty vector to disable this.

## Progress

- [x] 76ec2408e8fe3f1753c25db51efd8eb3
- [x] 0e6aa7be1f68d930926d72b3741a145c
- [x] 7997a3b2941eab92c1c0345d5747b420
- [x] 186f842951c0dcfe8838af1e7222b7d4
- [x] 2bf84e54b95ce97aefd9fc920451fc45
- [x] e09640936b3ef532b7b8e83ce8f125f4
- [ ] 4873cf6b76f62ac7d5a53605b2535a0c
- [ ] d0c54d4ed7f943280ce3e19532dbb1a6
